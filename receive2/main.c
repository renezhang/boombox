/**
 * Boombox
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

// === Config ==================================================================
// clock AND protoThreads configure!
#include "config.h"
// threading library
#include "pt_cornell_1_2.h"

// === Libraries ===============================================================
// Standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Spi Setup
#include "spi.h"

// Ring buffer library
#include "ring_buf.h"

// Audio hardware libraries
#include "dac.h"
#include "adc.h"

// === Thread Structures =======================================================
// thread control structs
static struct pt pt_screen;

// === Variables ===============================================================
// screen buffer
static char buf[80];

// iterator
static int i;

#define RING_SIZE 4096
// Circular ring buffer for audio
volatile static ring_buf_t ringL;
volatile static ring_buf_t ringR;

static int sinindex = 0;
static int sintable[256];

// === Timer 3 Interrupt Handler ===============================================
/*
 * Interrupt handler for timer 3.
 */

volatile int LR = 0;
volatile int readL;
volatile int readR;

void __ISR(_SPI_1_VECTOR, ipl1) SPI1InterruptHandler(void) {
  if (LR) {
//    if (!ring_empty(ringL)) {
//      readL = ((int) ring_pop(ringL)) - 32768;
//    }
//    SpiChnPutC(DAC_CHAN, readL << 8);
    SpiChnPutC(DAC_CHAN, sintable[sinindex] << 8);
  } else {
    SpiChnPutC(DAC_CHAN, sintable[sinindex] << 8);
//    if (!ring_empty(ringR)) {
//      readR = ((int) ring_pop(ringR)) - 32768;
//    }
//    SpiChnPutC(DAC_CHAN, readR << 8);    
  }
  LR = 1 - LR;
  sinindex++;
  if (sinindex >= 255) {
    sinindex = 0;
  }

  // Clear interrupt
  INTClearFlag(INT_SPI1TX);
}

// === Main  ===================================================================
/*
 * Set up TFT and other peripherals.
 * Set and schedule up protothreads.
 */
int main(void) {
  // === Setup chip ============================================================
  SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);

  ANSELA =0; //make sure analog is cleared
  ANSELB =0;

  // === Setup input ring buffer ===============================================
  ringL = ring_new(RING_SIZE);
  ringR = ring_new(RING_SIZE);
  
  for (i = 0; i < 256; i++) {
    sintable[i] = (int) (32000.0 * sin((float)i * 6.283 / 256.0));
  }
  
  // === Setup SPI =============================================================
  spi1_setup();
  spi2_setup();
  
  // === Setup DAC =============================================================
  dac_setup();
  
  // === Setup ADC =============================================================
  adc_setup();

  // === Setup protothreads ====================================================
//  PT_INIT(&pt_screen);

  // === setup system wide interrupts  =========================================
  INTEnableSystemMultiVectoredInt();

  // === Schedule the threads round robin ======================================
  while(1) {
//    PT_SCHEDULE(protothread_screen(&pt_screen));
//    transmit();
//    if (temp % 4 == 0) {
//      mPORTAToggleBits(BIT_0);
//    }
//    temp++;
  }
} // main
