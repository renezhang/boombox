/**
 * Header file for ADC control
 **/
#ifndef __ADC_H__
#define __ADC_H__

#include <plib.h>

/***** Setup Parameters *****/

#define ADC_FREQ 44100
#define ADC_CHAN SPI_CHANNEL2

#define ADC_CS_CLEAR() mPORTBClearBits(BIT_8)
#define ADC_CS_SET() mPORTBSetBits(BIT_8)

#define ADC_LEFT 0b1000000000000000
#define ADC_RIGHT 0b1100000000000000

/***** End Setup Parameter *****/

/***** Public Functions *****/

#define adc_read(side, data) { \
    ADC_CS_CLEAR(); \
    SpiChnWriteC(ADC_CHAN, side); \
    data = (unsigned short) SpiChnGetC(ADC_CHAN); \
    while (SPI2STATbits.SPIBUSY); \
    ADC_CS_SET(); \
}

// ADC Peripheral Setup
#define adc_setup() { \
    mPORTBSetPinsDigitalOut(BIT_8); \
    ADC_CS_SET(); \
}

/***** End Public Functions *****/

#endif  /* __ADC_H__ */

