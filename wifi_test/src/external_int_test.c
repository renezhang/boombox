//
//
//#include <plib.h>
//#include <stdio.h>
//#include "config.h"
//
//#include "tft_master.h"
//#include "tft_gfx.h"
//
//volatile int counter = 0;
//
//int main() {
//    
//    SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
////    mPORTBSetPinsDigitalIn(BIT_7);
//    EnablePullDownB(BIT_7);
//    
//    int last_counter = counter;
//    char buffer[10];
//    
//    mINT0SetEdgeMode(1);
//    INTEnable(INT_INT0, INT_ENABLED);
//    INTSetVectorPriority(INT_EXTERNAL_0_VECTOR, INT_PRIORITY_LEVEL_1);
//    CLI_Configure();
//    
//    sprintf(buffer, "counter = %d", counter);
//    CLI_Write(buffer);
//    
//    while (1) {
//        if (last_counter != counter) {
//            last_counter = counter;
//            sprintf(buffer, "counter = %d", counter);
//            CLI_Write(buffer);
//        }
//    }
//    
//    return -1;
//}
//
//void __ISR(_EXTERNAL_0_VECTOR, ipl1) counterHandler(void) {
//  counter++;
//  // Clear interrupt
//  mINT0ClearIntFlag();
//}