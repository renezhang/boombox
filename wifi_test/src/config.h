/*
 * File:   config.h
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

#ifndef __CONFIG_H__
#define	__CONFIG_H__
#define _SUPPRESS_PLIB_WARNINGS 1
#include <plib.h>
// serial stuff
#include <stdio.h>

//==============================================================================
// 40 MHz
#pragma config FNOSC = FRCPLL, POSCMOD = OFF
#pragma config FPLLIDIV = DIV_2, FPLLMUL = MUL_20 // 40 MHz
#pragma config FPBDIV = DIV_1, FPLLODIV = DIV_2 // PB 40MHz
#pragma config FWDTEN = OFF, JTAGEN = OFF, FSOSCEN = OFF, DEBUG = OFF

//==============================================================================
// Protothreads configure

// IF use_vref_debug IS defined, pin 25 is Vref output
//#define use_vref_debug

// IF use_uart_serial IS defined, pin 21 and pin 22 are used by the uart
// #define use_uart_serial
#define BAUDRATE 9600 // must match PC terminal emulator setting

//==============================================================================
// set up clock parameters
// system cpu clock
#define sys_clock 40000000

// sys_clock/FPBDIV
#define pb_clock sys_clock // divide by one in this case

//==============================================================================
// Pull Up and Pull Down resistor macros
// PORT B
#define EnablePullDownB(bits) CNPUBCLR=bits; CNPDBSET=bits;
#define DisablePullDownB(bits) CNPDBCLR=bits;
#define EnablePullUpB(bits) CNPDBCLR=bits; CNPUBSET=bits;
#define DisablePullUpB(bits) CNPUBCLR=bits;
//PORT A
#define EnablePullDownA(bits) CNPUACLR=bits; CNPDASET=bits;
#define DisablePullDownA(bits) CNPDACLR=bits;
#define EnablePullUpA(bits) CNPDACLR=bits; CNPUASET=bits;
#define DisablePullUpA(bits) CNPUACLR=bits;

#endif	/* __CONFIG_H__ */
