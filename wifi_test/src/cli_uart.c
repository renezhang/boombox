/**
 * Implementation of the "UART CLI" that reads and writes to the TFT instead
 */

#include "cli_uart.h"

#include "tft_master.h"
#include "tft_gfx.h"

#include <string.h>

// === TFT Macros ==============================================================
#define SIZE_TO_HEIGHT 9
#define tft_clearRect(x, y, w, h) { tft_fillRect(x, y, w, h, ILI9340_BLACK); }
#define tft_writeLine(x, y, size, s) { tft_setCursor(x, y); tft_setTextSize(size); tft_writeString(s); }
#define tft_rewriteLine(x, y, w, size, buff) { \
  tft_clearRect(x, y, w, size * SIZE_TO_HEIGHT); \
  tft_writeLine(x, y, size, buff); \
}

#define pb_clock 40000000
#define BAUDRATE 9600

void CLI_Configure(void) {
    
#ifdef TFT_ENABLED
  tft_init_hw();
  tft_begin();
  tft_fillScreen(ILI9340_BLACK);
  tft_setRotation(0); // 240x320 vertical display
  tft_setTextColor(ILI9340_WHITE);
#else // setup UART
   PPSInput (2, U2RX, RPB11); //Assign U2RX to pin RPB11 -- Physical pin 22 on 28 PDIP
 PPSOutput(4, RPB10, U2TX); //Assign U2TX to pin RPB10 -- Physical pin 21 on 28 PDIP
  UARTConfigure(UART2, UART_ENABLE_PINS_TX_RX_ONLY);
  UARTSetLineControl(UART2, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
  UARTSetDataRate(UART2, pb_clock, BAUDRATE);
  UARTEnable(UART2, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
#endif

}

static int yval;

int CLI_Write(unsigned char *inBuff) {
    
#ifdef TFT_ENABLED
  // Temporary hacky delay
    int i;
    for (i = 0; i < 100000; i++);
  tft_rewriteLine(0, yval, 240, 2, inBuff);
  yval = (yval + 20) % 300;
  return strlen(inBuff);
#else
  return printf("%s\n", inBuff);
#endif
  
}