/* ************************************************************************** */
/**

  @File Name
    cc3100.c

  @Description
    Contains the driver parameters for CC3100 SimpleLink library to function.
 */
/* ************************************************************************** */

#include "cc3100.h"

P_EVENT_HANDLER pIraEventHandler = 0;

channel_t cc3100_spi_open(char * protocol_name, unsigned long flags) {
    
    if (CC3100_SPI_CH == SPI_CHANNEL1) {
        mPORTBSetPinsDigitalIn(BIT_5);
        PPSInput(2, SDI1, RPB5);
        mPORTBSetPinsDigitalOut(BIT_1);
        PPSOutput(2, RPB1, SDO1);
    } else { // SPI_CH == SPI_CHANNEL2
        mPORTASetPinsDigitalIn(BIT_4);
        PPSInput(3, SDI2, RPA4);
        mPORTASetPinsDigitalOut(BIT_1);
        PPSOutput(2, RPA1, SDO2);
    }
    
    SpiChnOpen(
        CC3100_SPI_CH, 
        SPI_OPEN_ON | SPI_OPEN_MODE8 | SPI_OPEN_MSTEN,
        CC3100_SPI_DIV
    );
    
    return CC3100_SPI_CH;
    
}

int cc3100_spi_close(channel_t channel) {
    SpiChnClose(channel);
}

int cc3100_spi_read (channel_t channel, unsigned char * buffer, int len) {
    // Casts the char buffer to an int buffer
    int i;
    int temp[len];
    
    cc3100_cs_clear();
    
    for (i = 0; i < len; i++) {
        SpiChnPutC(channel, 0xFF);
        temp[i] = SpiChnGetC(channel);
    }
    
    while (SPI2STATbits.SPIBUSY);
    for (i = 0; i < len; i++) {
        buffer[i] = (char) temp[i];
    }
    cc3100_cs_set();
    
    // Debug print-out for read value
//    char *txt_buf[20];
//    for (i = 0; i < len; i++) {
//        sprintf(txt_buf, "READ: 0x%x", buffer[i]);
//        CLI_Write(txt_buf);
//    }
    
    return len;
}

int cc3100_spi_write(channel_t channel, unsigned char * buffer, int len) {
    // Casts the char buffer to an int buffer
    int i;
    
    cc3100_cs_clear();
    for (i = 0; i < len; i++) {
        SpiChnPutC(channel, (int) buffer[i]);
        SpiChnGetC(channel);
    }
    while (SPI2STATbits.SPIBUSY);
    cc3100_cs_set();
    
     // Debug print-out for read value
    char *txt_buf[20];
    for (i = 0; i < len; i++) {
//        sprintf(txt_buf, "SENT: 0x%x", buffer[i]);
//        CLI_Write(txt_buf);
    }
    
    return len;
}

int registerInterruptHandler(P_EVENT_HANDLER InterruptHdl , void* pValue)
{
    pIraEventHandler = InterruptHdl;

    return 0;
}

/*
 * External interrupt handler for CC3100 IRQ.
 */
void __ISR(_EXTERNAL_0_VECTOR, ipl1) cc3100IRQHandler(void) {
  if (pIraEventHandler)  
    pIraEventHandler(0);
  // Clear interrupt
  mINT0ClearIntFlag();
}