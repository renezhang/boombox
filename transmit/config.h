/*
 * General configuration file for the system.
 * 
 * File:   config.h
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

#ifndef __CONFIG_H__
#define	__CONFIG_H__
#define _SUPPRESS_PLIB_WARNINGS 1
#include <plib.h>
// serial stuff
#include <stdio.h>

// Protothreads configure

// IF use_vref_debug IS defined, pin 25 is Vref output
//#define use_vref_debug

// IF use_uart_serial IS defined, pin 21 and pin 22 are used by the uart
// #define use_uart_serial
#define BAUDRATE 9600 // must match PC terminal emulator setting

//==============================================================================

/*
 * WARNING: For the system to operate correctly, after values in this section
 *          have been changed, directives in mcu_config.h must be modified
 *          accordingly.
 */

// set up clock parameters
// system cpu clock
#define sys_clock 40000000

// sys_clock/FPBDIV
#define pb_clock sys_clock // divide by one in this case

//==============================================================================
// Pull Up and Pull Down resistor macros
// PORT B
#define EnablePullDownB(bits) CNPUBCLR=bits; CNPDBSET=bits;
#define DisablePullDownB(bits) CNPDBCLR=bits;
#define EnablePullUpB(bits) CNPDBCLR=bits; CNPUBSET=bits;
#define DisablePullUpB(bits) CNPUBCLR=bits;
//PORT A
#define EnablePullDownA(bits) CNPUACLR=bits; CNPDASET=bits;
#define DisablePullDownA(bits) CNPDACLR=bits;
#define EnablePullUpA(bits) CNPDACLR=bits; CNPUASET=bits;
#define DisablePullUpA(bits) CNPUACLR=bits;

#endif	/* __CONFIG_H__ */
