/**
 * Header file for SPI Setup
 **/
#ifndef __SPI_H__
#define __SPI_H__

#include <plib.h>

/***** Setup Parameters *****/

// Ratio between sampling rate and clock rate
#define SPI_DIV 2

/***** End Setup Parameter *****/

/***** Public Functions *****/

// Wired data transmission channel
// SCK1 - pin 25
// SDO1 - pin 22
#define spi1_setup() { \
    mPORTBSetPinsDigitalOut(BIT_11); \
    PPSOutput(2, RPB11, SDO1); \
    SpiChnOpen(SPI_CHANNEL1, SPI_OPEN_ON | SPI_OPEN_MODE32 | SPI_OPEN_MSTEN | SPI_OPEN_DISSDI , SPI_DIV); \
}

// ADC channel
// SCK2 - pin 26
// SDI2 - pin 12
// SDO2 - pin 3
#define spi2_setup() { \
    mPORTASetPinsDigitalIn(BIT_4); \
    PPSInput(3, SDI2, RPA4); \
    mPORTASetPinsDigitalOut(BIT_1); \
    PPSOutput(2, RPA1, SDO2); \
    SpiChnOpen(SPI_CHANNEL2, SPI_OPEN_ON | SPI_OPEN_MODE16 | SPI_OPEN_MSTEN | SPI_OPEN_CKE_REV, SPI_DIV); \
}

/***** End Public Functions *****/

#endif  /* __SPI_H__ */
