/**
 * Header file for a circular ring buffer
 * Contains unsigned shorts
 **/
#ifndef __RING_H__
#define __RING_H__

typedef struct ring_buf* ring_buf_t;
typedef unsigned short out_t; // 16-bit

extern ring_buf_t ring_new(unsigned int size);
extern void ring_free(ring_buf_t);
extern unsigned int ring_empty(ring_buf_t);
extern unsigned int ring_size(ring_buf_t);
extern unsigned int ring_max_size(ring_buf_t);
extern void ring_push(ring_buf_t, out_t);
extern out_t ring_pop(ring_buf_t);

#endif  /* __RING_H__ */
