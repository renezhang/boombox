/* ************************************************************************** */
/**

  @File Name
    cc3100.h

  @Description
    Contains the driver parameters for CC3100 SimpleLink library to function.
 */
/* ************************************************************************** */

#ifndef __CC3100_H__    /* Guard against multiple inclusion */
#define __CC3100_H__

#include <plib.h>
#include "config.h"
#include "cli_uart.h"

#define RISING_INT_EDGE 1

/* SPI defines */

typedef unsigned short channel_t;

#define CC3100_SPI_DIV 2
#define CC3100_SPI_CH  SPI_CHANNEL1

/* Event handler defines */

typedef void (*P_EVENT_HANDLER)(void* pValue);

/*
 * Pin Mappings:
 * 
 * Reset       - RB13 (Pin 24)
 * Chip Select - RB9  (Pin 18)
 * MISO (SPI1) - RB1  (Pin  5)
 * MOSI (SPI1) - RB2  (Pin  6)
 * MISO (SPI2) - RA4  (Pin 12)
 * MOSI (SPI2) - RA1  (Pin  3)
 * SCK (SPI1)  - SCK1 (Pin 25)
 * SCK (SPI2)  - SCK2 (Pin 26)
 */

#define cc3100_setup() { \
    SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE); \
    ANSELA = 0; \
    ANSELB = 0; \
    mPORTBSetPinsDigitalOut(BIT_13); \
    mPORTBSetBits(BIT_13); \
    mPORTBSetPinsDigitalOut(BIT_9); \
    mPORTBSetBits(BIT_9); \
    mINT0SetEdgeMode(RISING_INT_EDGE); \
    EnablePullDownB(BIT_7); \
    INTSetVectorPriority(INT_EXTERNAL_0_VECTOR, INT_PRIORITY_LEVEL_1); \
    INTEnable(INT_INT0, INT_ENABLED); \
    mINT0ClearIntFlag(); \
    INTEnableSystemMultiVectoredInt(); \
}

#define cc3100_enable()  mPORTBSetBits(BIT_13)

#define cc3100_disable() { \
    mPORTBClearBits(BIT_13); \
    int i; \
    for (i = 0; i < 1000; i++); \
}

/* SPI Functions */

channel_t cc3100_spi_open (char * protocol_name, unsigned long flags);
int       cc3100_spi_close(channel_t channel);
int       cc3100_spi_read (channel_t channel, unsigned char * buffer, int len);
int       cc3100_spi_write(channel_t channel, unsigned char * buffer, int len);

/*
 * Sets signal on chip select line.
 */
#define cc3100_cs_set()   mPORTBSetBits(BIT_9)

/*
 * Clears signal on chip select line.
 */
#define cc3100_cs_clear() mPORTBClearBits(BIT_9)

/* Event handler functions */

int registerInterruptHandler(P_EVENT_HANDLER InterruptHdl , void* pValue);

#endif /* __CC3100_H__ */