/**
 * Boombox
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

// === Config ==================================================================
// clock AND protoThreads configure!
#include "config.h"
#include "mcu_config.h"

// === Libraries ===============================================================
// Standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Spi Setup Library
#include "spi.h"

// Ring buffer library
#include "ring_buf.h"

// Audio hardware libraries
#include "adc.h"

// Compression Library
#include "adpcm.h"

// UDP library
#include "udp.h"

//#define ADPCM

// === Variables ===============================================================
// iterator
static int i;

#define RING_SIZE 4096
// Circular ring buffer for audio
volatile static ring_buf_t ringL;
volatile static ring_buf_t ringR;

// === Transmission ============================================================
#define TRANS_CHAN SPI_CHANNEL1
#define PKT_SIZE 256
#define SYNC_NUM 4
#define SYNC_DATA 0xDE

void send_packet(unsigned int * pkt) {
  for (i = 0; i < SYNC_NUM; i++) {
    SpiChnPutC(TRANS_CHAN, SYNC_DATA);
    while (SpiChnIsBusy(TRANS_CHAN));
  }
  for (i = 0; i < PKT_SIZE; i++) {
    SpiChnPutC(TRANS_CHAN, pkt[i]);
    while (SpiChnIsBusy(TRANS_CHAN));
  }
}

// TODO remove
    unsigned short sine_table[64] = {
32768,36030,39260,42426,45496,
48439,51226,53830,56225,58386,
60293,61926,63270,64310,65037,
65443,65525,65281,64713,63829,
62635,61145,59373,57336,55055,
52553,49854,46985,43975,40853,
37651,34401,31134,27884,24682,
21560,18550,15681,12982,10480,
8199,6162,4390,2900,1706,
822,254,10,92,498,
1225,2265,3609,5242,7149,
9310,11705,14309,17096,20039,
23109,26275,29505,32768,
    };

void transmit() {
//  unsigned char pkt[PKT_SIZE];
//  unsigned char data;
//  for (i = 0; i < PKT_SIZE; i++) {
//    data = (ring_pop(ringL) << 4) | ring_pop(ringR);
//    pkt[i] = data;
//  }
  unsigned int pkt[PKT_SIZE];
  unsigned short data_left, data_right;
  unsigned int data;
  for (i = 0; i < PKT_SIZE; i++) {
      
#ifdef ADPCM
      data_left = (unsigned short) (ring_pop(ringL) & 0xF) << 12 |
                  (unsigned short) (ring_pop(ringL) & 0xF) <<  8 |
                  (unsigned short) (ring_pop(ringL) & 0xF) <<  4 |
                  (unsigned short) (ring_pop(ringL) & 0xF);
      data_right = (unsigned short) (ring_pop(ringR) & 0xF) << 12 |
                   (unsigned short) (ring_pop(ringR) & 0xF) <<  8 |
                   (unsigned short) (ring_pop(ringR) & 0xF) <<  4 |
                   (unsigned short) (ring_pop(ringR) & 0xF);
#else
    // 32-bit data transmission; combining both stereo channels into 1 p`kt
    data_left  = ring_pop(ringL);
    data_right = ring_pop(ringR);
//    data_left = sine_table[i%64];
//    data_right = sine_table[i%64];
#endif
    
    data = (((unsigned int) data_left) << 16) | (unsigned int) data_right;
    pkt[i] = data;
  }
//  send_packet(pkt); // SPI wired send
  udp_send_packet(pkt, PKT_SIZE);
}

// === Timer 3 Interrupt Handler ===============================================
/*
 * Interrupt handler for timer 3.
 */

volatile int ADC_LR = 0;
volatile unsigned short data;

// Pointers for encoding on left side
volatile unsigned long ps_encL = 0;
volatile int pi_encL = 0;
// Pointers for encoding on right side
volatile unsigned long ps_encR = 0;
volatile int pi_encR = 0;

void __ISR(_TIMER_3_VECTOR, ipl2) Timer3Handler(void) {
  if (ADC_LR) {
    adc_read(ADC_LEFT, data);
#ifdef ADPCM
    ring_push(ringL, ADPCMEncoder((signed long) data - 32768, &ps_encL, &pi_encL));
#else
    ring_push(ringL, data);
#endif
  } else {
    adc_read(ADC_RIGHT, data);
#ifdef ADPCM
    ring_push(ringR, ADPCMEncoder((signed long) data - 32768, &ps_encR, &pi_encR));
#else
    ring_push(ringR, data);
#endif
  }
  ADC_LR = 1 - ADC_LR;
  
  // Clear interrupt
  mT3ClearIntFlag();
}

// === Main  ===================================================================
/*
 * Set up TFT and other peripherals.
 * Set and schedule up protothreads.
 */
int main(void) {
  // === Setup chip ============================================================
  SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);

  ANSELA =0; //make sure analog is cleared
  ANSELB =0;

  // === Setup input ring buffer ===============================================
  ringL = ring_new(RING_SIZE);
  ringR = ring_new(RING_SIZE);
  
  // === Setup timer3 ==========================================================
  OpenTimer3(T3_ON | T3_SOURCE_INT | T3_PS_1_1, sys_clock / ADC_FREQ / 2);

  ConfigIntTimer3(T3_INT_ON | T3_INT_PRIOR_2);
  mT3ClearIntFlag();

  // === Setup SPI =============================================================
//  spi1_setup();
  spi2_setup();
  
  // === Setup ADC =============================================================
  adc_setup();
  
  // === Setup UDP =============================================================
  udp_send_setup();

  // === setup system wide interrupts  =========================================
  INTEnableSystemMultiVectoredInt();
  
  // TODO remove
  int i;

  mPORTASetPinsDigitalOut(BIT_0);
  mPORTASetBits(BIT_0);
  int blink = 0;
  
  CLI_Write("Transmission start");
  
  while(1) {
    while (ring_size(ringL) < PKT_SIZE && ring_size(ringR) < PKT_SIZE); // TODO PUT BACK!!!

    transmit();
    if (blink++ % 20 == 0) {
      mPORTAToggleBits(BIT_0);
    }
  }
} // main
