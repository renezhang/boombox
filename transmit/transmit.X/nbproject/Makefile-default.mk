#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../simplelink/device.c ../simplelink/driver.c ../simplelink/flowcont.c ../simplelink/fs.c ../simplelink/netapp.c ../simplelink/netcfg.c ../simplelink/nonos.c ../simplelink/socket.c ../simplelink/spawn.c ../simplelink/wlan.c ../glcdfont.c ../tft_gfx.c ../tft_master.c ../ring_buf.c ../main.c ../cc3100.c ../cli_uart.c ../udp.c ../udp_test.c ../../receive/adpcm.c ../spi_test_main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1042967643/device.o ${OBJECTDIR}/_ext/1042967643/driver.o ${OBJECTDIR}/_ext/1042967643/flowcont.o ${OBJECTDIR}/_ext/1042967643/fs.o ${OBJECTDIR}/_ext/1042967643/netapp.o ${OBJECTDIR}/_ext/1042967643/netcfg.o ${OBJECTDIR}/_ext/1042967643/nonos.o ${OBJECTDIR}/_ext/1042967643/socket.o ${OBJECTDIR}/_ext/1042967643/spawn.o ${OBJECTDIR}/_ext/1042967643/wlan.o ${OBJECTDIR}/_ext/1472/glcdfont.o ${OBJECTDIR}/_ext/1472/tft_gfx.o ${OBJECTDIR}/_ext/1472/tft_master.o ${OBJECTDIR}/_ext/1472/ring_buf.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/cc3100.o ${OBJECTDIR}/_ext/1472/cli_uart.o ${OBJECTDIR}/_ext/1472/udp.o ${OBJECTDIR}/_ext/1472/udp_test.o ${OBJECTDIR}/_ext/1875618115/adpcm.o ${OBJECTDIR}/_ext/1472/spi_test_main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1042967643/device.o.d ${OBJECTDIR}/_ext/1042967643/driver.o.d ${OBJECTDIR}/_ext/1042967643/flowcont.o.d ${OBJECTDIR}/_ext/1042967643/fs.o.d ${OBJECTDIR}/_ext/1042967643/netapp.o.d ${OBJECTDIR}/_ext/1042967643/netcfg.o.d ${OBJECTDIR}/_ext/1042967643/nonos.o.d ${OBJECTDIR}/_ext/1042967643/socket.o.d ${OBJECTDIR}/_ext/1042967643/spawn.o.d ${OBJECTDIR}/_ext/1042967643/wlan.o.d ${OBJECTDIR}/_ext/1472/glcdfont.o.d ${OBJECTDIR}/_ext/1472/tft_gfx.o.d ${OBJECTDIR}/_ext/1472/tft_master.o.d ${OBJECTDIR}/_ext/1472/ring_buf.o.d ${OBJECTDIR}/_ext/1472/main.o.d ${OBJECTDIR}/_ext/1472/cc3100.o.d ${OBJECTDIR}/_ext/1472/cli_uart.o.d ${OBJECTDIR}/_ext/1472/udp.o.d ${OBJECTDIR}/_ext/1472/udp_test.o.d ${OBJECTDIR}/_ext/1875618115/adpcm.o.d ${OBJECTDIR}/_ext/1472/spi_test_main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1042967643/device.o ${OBJECTDIR}/_ext/1042967643/driver.o ${OBJECTDIR}/_ext/1042967643/flowcont.o ${OBJECTDIR}/_ext/1042967643/fs.o ${OBJECTDIR}/_ext/1042967643/netapp.o ${OBJECTDIR}/_ext/1042967643/netcfg.o ${OBJECTDIR}/_ext/1042967643/nonos.o ${OBJECTDIR}/_ext/1042967643/socket.o ${OBJECTDIR}/_ext/1042967643/spawn.o ${OBJECTDIR}/_ext/1042967643/wlan.o ${OBJECTDIR}/_ext/1472/glcdfont.o ${OBJECTDIR}/_ext/1472/tft_gfx.o ${OBJECTDIR}/_ext/1472/tft_master.o ${OBJECTDIR}/_ext/1472/ring_buf.o ${OBJECTDIR}/_ext/1472/main.o ${OBJECTDIR}/_ext/1472/cc3100.o ${OBJECTDIR}/_ext/1472/cli_uart.o ${OBJECTDIR}/_ext/1472/udp.o ${OBJECTDIR}/_ext/1472/udp_test.o ${OBJECTDIR}/_ext/1875618115/adpcm.o ${OBJECTDIR}/_ext/1472/spi_test_main.o

# Source Files
SOURCEFILES=../simplelink/device.c ../simplelink/driver.c ../simplelink/flowcont.c ../simplelink/fs.c ../simplelink/netapp.c ../simplelink/netcfg.c ../simplelink/nonos.c ../simplelink/socket.c ../simplelink/spawn.c ../simplelink/wlan.c ../glcdfont.c ../tft_gfx.c ../tft_master.c ../ring_buf.c ../main.c ../cc3100.c ../cli_uart.c ../udp.c ../udp_test.c ../../receive/adpcm.c ../spi_test_main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F128B
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1042967643/device.o: ../simplelink/device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/device.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/device.o.d" -o ${OBJECTDIR}/_ext/1042967643/device.o ../simplelink/device.c     
	
${OBJECTDIR}/_ext/1042967643/driver.o: ../simplelink/driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/driver.o.d" -o ${OBJECTDIR}/_ext/1042967643/driver.o ../simplelink/driver.c     
	
${OBJECTDIR}/_ext/1042967643/flowcont.o: ../simplelink/flowcont.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/flowcont.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/flowcont.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/flowcont.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/flowcont.o.d" -o ${OBJECTDIR}/_ext/1042967643/flowcont.o ../simplelink/flowcont.c     
	
${OBJECTDIR}/_ext/1042967643/fs.o: ../simplelink/fs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/fs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/fs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/fs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/fs.o.d" -o ${OBJECTDIR}/_ext/1042967643/fs.o ../simplelink/fs.c     
	
${OBJECTDIR}/_ext/1042967643/netapp.o: ../simplelink/netapp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netapp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netapp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/netapp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/netapp.o.d" -o ${OBJECTDIR}/_ext/1042967643/netapp.o ../simplelink/netapp.c     
	
${OBJECTDIR}/_ext/1042967643/netcfg.o: ../simplelink/netcfg.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netcfg.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netcfg.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/netcfg.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/netcfg.o.d" -o ${OBJECTDIR}/_ext/1042967643/netcfg.o ../simplelink/netcfg.c     
	
${OBJECTDIR}/_ext/1042967643/nonos.o: ../simplelink/nonos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/nonos.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/nonos.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/nonos.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/nonos.o.d" -o ${OBJECTDIR}/_ext/1042967643/nonos.o ../simplelink/nonos.c     
	
${OBJECTDIR}/_ext/1042967643/socket.o: ../simplelink/socket.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/socket.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/socket.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/socket.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/socket.o.d" -o ${OBJECTDIR}/_ext/1042967643/socket.o ../simplelink/socket.c     
	
${OBJECTDIR}/_ext/1042967643/spawn.o: ../simplelink/spawn.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/spawn.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/spawn.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/spawn.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/spawn.o.d" -o ${OBJECTDIR}/_ext/1042967643/spawn.o ../simplelink/spawn.c     
	
${OBJECTDIR}/_ext/1042967643/wlan.o: ../simplelink/wlan.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/wlan.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/wlan.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/wlan.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/wlan.o.d" -o ${OBJECTDIR}/_ext/1042967643/wlan.o ../simplelink/wlan.c     
	
${OBJECTDIR}/_ext/1472/glcdfont.o: ../glcdfont.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/glcdfont.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/glcdfont.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/glcdfont.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/glcdfont.o.d" -o ${OBJECTDIR}/_ext/1472/glcdfont.o ../glcdfont.c     
	
${OBJECTDIR}/_ext/1472/tft_gfx.o: ../tft_gfx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_gfx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_gfx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/tft_gfx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/tft_gfx.o.d" -o ${OBJECTDIR}/_ext/1472/tft_gfx.o ../tft_gfx.c     
	
${OBJECTDIR}/_ext/1472/tft_master.o: ../tft_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_master.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/tft_master.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/tft_master.o.d" -o ${OBJECTDIR}/_ext/1472/tft_master.o ../tft_master.c     
	
${OBJECTDIR}/_ext/1472/ring_buf.o: ../ring_buf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/ring_buf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ring_buf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ring_buf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ring_buf.o.d" -o ${OBJECTDIR}/_ext/1472/ring_buf.o ../ring_buf.c     
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d" -o ${OBJECTDIR}/_ext/1472/main.o ../main.c     
	
${OBJECTDIR}/_ext/1472/cc3100.o: ../cc3100.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/cc3100.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/cc3100.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/cc3100.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/cc3100.o.d" -o ${OBJECTDIR}/_ext/1472/cc3100.o ../cc3100.c     
	
${OBJECTDIR}/_ext/1472/cli_uart.o: ../cli_uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/cli_uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/cli_uart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/cli_uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/cli_uart.o.d" -o ${OBJECTDIR}/_ext/1472/cli_uart.o ../cli_uart.c     
	
${OBJECTDIR}/_ext/1472/udp.o: ../udp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/udp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/udp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/udp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/udp.o.d" -o ${OBJECTDIR}/_ext/1472/udp.o ../udp.c     
	
${OBJECTDIR}/_ext/1472/udp_test.o: ../udp_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/udp_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/udp_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/udp_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/udp_test.o.d" -o ${OBJECTDIR}/_ext/1472/udp_test.o ../udp_test.c     
	
${OBJECTDIR}/_ext/1875618115/adpcm.o: ../../receive/adpcm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1875618115" 
	@${RM} ${OBJECTDIR}/_ext/1875618115/adpcm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1875618115/adpcm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1875618115/adpcm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1875618115/adpcm.o.d" -o ${OBJECTDIR}/_ext/1875618115/adpcm.o ../../receive/adpcm.c     
	
${OBJECTDIR}/_ext/1472/spi_test_main.o: ../spi_test_main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/spi_test_main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/spi_test_main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/spi_test_main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/spi_test_main.o.d" -o ${OBJECTDIR}/_ext/1472/spi_test_main.o ../spi_test_main.c     
	
else
${OBJECTDIR}/_ext/1042967643/device.o: ../simplelink/device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/device.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/device.o.d" -o ${OBJECTDIR}/_ext/1042967643/device.o ../simplelink/device.c     
	
${OBJECTDIR}/_ext/1042967643/driver.o: ../simplelink/driver.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/driver.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/driver.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/driver.o.d" -o ${OBJECTDIR}/_ext/1042967643/driver.o ../simplelink/driver.c     
	
${OBJECTDIR}/_ext/1042967643/flowcont.o: ../simplelink/flowcont.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/flowcont.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/flowcont.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/flowcont.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/flowcont.o.d" -o ${OBJECTDIR}/_ext/1042967643/flowcont.o ../simplelink/flowcont.c     
	
${OBJECTDIR}/_ext/1042967643/fs.o: ../simplelink/fs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/fs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/fs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/fs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/fs.o.d" -o ${OBJECTDIR}/_ext/1042967643/fs.o ../simplelink/fs.c     
	
${OBJECTDIR}/_ext/1042967643/netapp.o: ../simplelink/netapp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netapp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netapp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/netapp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/netapp.o.d" -o ${OBJECTDIR}/_ext/1042967643/netapp.o ../simplelink/netapp.c     
	
${OBJECTDIR}/_ext/1042967643/netcfg.o: ../simplelink/netcfg.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netcfg.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/netcfg.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/netcfg.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/netcfg.o.d" -o ${OBJECTDIR}/_ext/1042967643/netcfg.o ../simplelink/netcfg.c     
	
${OBJECTDIR}/_ext/1042967643/nonos.o: ../simplelink/nonos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/nonos.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/nonos.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/nonos.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/nonos.o.d" -o ${OBJECTDIR}/_ext/1042967643/nonos.o ../simplelink/nonos.c     
	
${OBJECTDIR}/_ext/1042967643/socket.o: ../simplelink/socket.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/socket.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/socket.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/socket.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/socket.o.d" -o ${OBJECTDIR}/_ext/1042967643/socket.o ../simplelink/socket.c     
	
${OBJECTDIR}/_ext/1042967643/spawn.o: ../simplelink/spawn.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/spawn.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/spawn.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/spawn.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/spawn.o.d" -o ${OBJECTDIR}/_ext/1042967643/spawn.o ../simplelink/spawn.c     
	
${OBJECTDIR}/_ext/1042967643/wlan.o: ../simplelink/wlan.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1042967643" 
	@${RM} ${OBJECTDIR}/_ext/1042967643/wlan.o.d 
	@${RM} ${OBJECTDIR}/_ext/1042967643/wlan.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1042967643/wlan.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1042967643/wlan.o.d" -o ${OBJECTDIR}/_ext/1042967643/wlan.o ../simplelink/wlan.c     
	
${OBJECTDIR}/_ext/1472/glcdfont.o: ../glcdfont.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/glcdfont.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/glcdfont.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/glcdfont.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/glcdfont.o.d" -o ${OBJECTDIR}/_ext/1472/glcdfont.o ../glcdfont.c     
	
${OBJECTDIR}/_ext/1472/tft_gfx.o: ../tft_gfx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_gfx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_gfx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/tft_gfx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/tft_gfx.o.d" -o ${OBJECTDIR}/_ext/1472/tft_gfx.o ../tft_gfx.c     
	
${OBJECTDIR}/_ext/1472/tft_master.o: ../tft_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/tft_master.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/tft_master.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/tft_master.o.d" -o ${OBJECTDIR}/_ext/1472/tft_master.o ../tft_master.c     
	
${OBJECTDIR}/_ext/1472/ring_buf.o: ../ring_buf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/ring_buf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/ring_buf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/ring_buf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/ring_buf.o.d" -o ${OBJECTDIR}/_ext/1472/ring_buf.o ../ring_buf.c     
	
${OBJECTDIR}/_ext/1472/main.o: ../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/main.o.d" -o ${OBJECTDIR}/_ext/1472/main.o ../main.c     
	
${OBJECTDIR}/_ext/1472/cc3100.o: ../cc3100.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/cc3100.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/cc3100.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/cc3100.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/cc3100.o.d" -o ${OBJECTDIR}/_ext/1472/cc3100.o ../cc3100.c     
	
${OBJECTDIR}/_ext/1472/cli_uart.o: ../cli_uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/cli_uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/cli_uart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/cli_uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/cli_uart.o.d" -o ${OBJECTDIR}/_ext/1472/cli_uart.o ../cli_uart.c     
	
${OBJECTDIR}/_ext/1472/udp.o: ../udp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/udp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/udp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/udp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/udp.o.d" -o ${OBJECTDIR}/_ext/1472/udp.o ../udp.c     
	
${OBJECTDIR}/_ext/1472/udp_test.o: ../udp_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/udp_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/udp_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/udp_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/udp_test.o.d" -o ${OBJECTDIR}/_ext/1472/udp_test.o ../udp_test.c     
	
${OBJECTDIR}/_ext/1875618115/adpcm.o: ../../receive/adpcm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1875618115" 
	@${RM} ${OBJECTDIR}/_ext/1875618115/adpcm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1875618115/adpcm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1875618115/adpcm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1875618115/adpcm.o.d" -o ${OBJECTDIR}/_ext/1875618115/adpcm.o ../../receive/adpcm.c     
	
${OBJECTDIR}/_ext/1472/spi_test_main.o: ../spi_test_main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/spi_test_main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/spi_test_main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/spi_test_main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1472/spi_test_main.o.d" -o ${OBJECTDIR}/_ext/1472/spi_test_main.o ../spi_test_main.c     
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}              -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,--defsym=_min_heap_size=20000,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}            -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=20000,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/transmit.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
