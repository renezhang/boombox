/**
 * Boombox
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

// === Config ==================================================================
// clock AND protoThreads configure!
#include "config.h"

// === Libraries ===============================================================
// Standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Spi Setup
#include "spi.h"

// Ring buffer library
#include "ring_buf.h"

// Audio hardware libraries
#include "dac.h"

// Audio data receive library
#include "audio_receive.h"

// === Variables ===============================================================

// iterator
static int i;

#define RING_SIZE 4096
// Circular ring buffer for audio
volatile static ring_buf_t ringL;
volatile static ring_buf_t ringR;

static int sinindex = 0;
static int sintable[256];

/* DAC output interrupt handler */

volatile unsigned short data;

volatile int DAC_LR = 0;
volatile int readL = 0;
volatile int readR = 0;

void __ISR(_SPI_1_VECTOR, ipl1) SPI1InterruptHandler(void) {
  if (DAC_LR) {
    if (!ring_empty(ringL)) {
      readL = ((int) ring_pop(ringL)) - 32768;
    }
    SpiChnPutC(DAC_CHAN, readL << 8);
//    SpiChnPutC(DAC_CHAN, sintable[sinindex] << 8);
  } else {
//    SpiChnPutC(DAC_CHAN, sintable[sinindex] << 8);
    if (!ring_empty(ringR)) {
      readR = ((int) ring_pop(ringR)) - 32768;
    }
    SpiChnPutC(DAC_CHAN, readR << 8);    
  }
  DAC_LR = 1 - DAC_LR;
//  sinindex++;
//  if (sinindex >= 255) {
//    sinindex = 0;
//  }
//    if (!ring_empty(ring)) {
//      read = ((int) ring_pop(ring)) - 32768;
//    }
//    SpiChnPutC(DAC_CHAN, read << 8);
  // Clear interrupt
  INTClearFlag(INT_SPI1TX);
}

// === Main  ===================================================================
/*
 * Set up TFT and other peripherals.
 * Set and schedule up protothreads.
 */
int main(void) {
    
  SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
  
  ANSELA =0; //make sure analog is cleared
  ANSELB =0;
  
  // TODO remove, for spi slave mode debugging only.
  mPORTASetPinsDigitalOut(BIT_0);
  mPORTAClearBits(BIT_0);
    
//   === Setup input ring buffer ===============================================
  ringL = ring_new(RING_SIZE);
  ringR = ring_new(RING_SIZE);
  
  for (i = 0; i < 256; i++) {
    sintable[i] = (int) (32000.0 * sin((float)i * 6.283 / 256.0));
  }

  // === Setup SPI =============================================================
  spi1_setup();
  
  // === Setup DAC =============================================================
  dac_setup();
  
  // === Setup receiver system =================================================
  ar_setup();

  // === setup system wide interrupts  =========================================
//  INTEnableSystemMultiVectoredInt();
  
  // Main data reception loop

  unsigned short left_buf[AR_CHN_PKT_SIZE];
  unsigned short right_buf[AR_CHN_PKT_SIZE];
  
  while (1) {
      
//      mPORTAToggleBits(BIT_0);
//      for( i = 0; i < 1000000; i++);
      ar_receive(left_buf, right_buf);
      mPORTASetBits(BIT_0);
      
      int i;
      for (i = 0; i < AR_CHN_PKT_SIZE; i++) {
          ring_push(ringL, sintable[i % 256]);
          ring_push(ringR, sintable[i % 256]);
//        ring_push(ringL, left_buf[i] );
//        ring_push(ringR, right_buf[i]);
      }
      
//      if (received == 0xAA)
//        mPORTASetBits(BIT_0);
//      for (i = 0; i < 100000; i++);
//      mPORTAClearBits(BIT_0);
  }
  
} // main
