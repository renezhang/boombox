/**
 * Audio data receive implementation
 **/
#include "audio_receive.h"
#include "spi.h"

/* Constants */

// AR_SYNC_WORD repeated AR_SYNC_REP times marks the start of a stereo packet.
#define AR_SYNC_WORD 0xDE 
#define AR_SYNC_REP  4

/* Public functions */

void ar_setup() {
    spi2_setup();
}


void ar_receive(unsigned short *left_buffer, unsigned short *right_buffer) {
        
    // Check for the start of a packet
    int sync_count = 0;
    int received;
    while (sync_count < AR_SYNC_REP) {
        SpiChnGetRov(REC_CHANNEL, 1);
        received = SpiChnGetC(REC_CHANNEL);
        if ( received == AR_SYNC_WORD)
            sync_count++;
        else
            sync_count = 0;
    }
    
    // Read data into the two given buffers.
    // First half of the packet is for the left buffer, and second half is for
    // the right buffer.
    int i;
    for ( i = 0; i < AR_CHN_PKT_SIZE * 2; i++) {
        unsigned short msb = (unsigned short) SpiChnGetC(REC_CHANNEL);
        unsigned short lsb = (unsigned short) SpiChnGetC(REC_CHANNEL);
        if (i < AR_CHN_PKT_SIZE) {
            left_buffer[i]                  = (msb << 8) | lsb;
        }
        else {
            right_buffer[i-AR_CHN_PKT_SIZE] = (msb << 8) | lsb;
        }
    }
    
//    int buf[AR_CHN_PKT_SIZE*4];
//    int i;
//    for ( i = 0; i < AR_CHN_PKT_SIZE*4; i++ )
//        buf[i] = SpiChnGetC(REC_CHANNEL);
}
