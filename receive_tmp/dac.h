/**
 * Header file for DAC control
 **/
#ifndef __DAC_H__
#define __DAC_H__

#include <plib.h>

/***** Setup Parameters *****/

#define DAC_FREQ 44100
#define DAC_CHAN SPI_CHANNEL1

#define DAC_CS_CLEAR() mPORTBClearBits(BIT_4)
#define DAC_CS_SET() mPORTBSetBits(BIT_4)

// A-channel, 1x, active
#define DAC_config_chan_A 0b0011000000000000

/***** End Setup Parameter *****/

/***** Public Functions *****/

// RPB13 - REFCLKO 11.2896 MHz
#define dac_setup() { \
    mPORTBSetPinsDigitalOut(BIT_13); \
    PPSOutput(3, RPB13, REFCLKO); \
    mOSCREFOTRIMSet(44); \
    OSCREFConfig(OSC_REFOCON_SYSPLL, OSC_REFOCON_ON | OSC_REFOCON_OE, 7); \
}

/***** End Public Functions *****/

#endif  /* __DAC_H__ */
