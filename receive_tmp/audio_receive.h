/**
 * Header file for audio data receive
 **/
#ifndef __AUDIO_RECEIVE_H__
#define __AUDIO_RECEIVE_H__

#include <plib.h>

/***** Public Constants *****/

// Number of bytes for each stereo channel.
// Total packet size is double this amount.
#define AR_CHN_PKT_SIZE 256 

/***** Public Functions *****/

void ar_setup();

/**
 * 
 */
void ar_receive(unsigned short *left_buffer, unsigned short *right_buffer);

/***** End Public Functions *****/

#endif  /* __AUDIO_RECEIVE_H__ */
