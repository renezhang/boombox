#include "config.h"

#include "spi.h"
#include "ring_buf.h"
#include "audio_receive.h"
#include "dac.h"

#define RING_SIZE 4096
// Circular ring buffer for audio
volatile static ring_buf_t ringL;
volatile static ring_buf_t ringR;

volatile int DAC_LR = 0;
volatile int readL = 0;
volatile int readR = 0;

// Heartbeat
volatile int led_count = 0;

volatile static int sinindex = 0;
static int sintable[256];

void __ISR(_SPI_1_VECTOR, ipl4) SPI1InterruptHandler(void) {
  if (DAC_LR) {
    if (!ring_empty(ringL)) {
      readL = ((int) ring_pop(ringL)) - 32768;
    }
    SpiChnPutC(DAC_CHAN, readL << 8);
  } else {
    if (!ring_empty(ringR)) {
      readR = ((int) ring_pop(ringR)) - 32768;
    }
    SpiChnPutC(DAC_CHAN, readR << 8);    
  }
  DAC_LR = 1 - DAC_LR;
  // Clear interrupt
  INTClearFlag(INT_SPI1TX);
}

void main() {
    
    SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
  
    ANSELA =0; //make sure analog is cleared
    ANSELB =0;
    
    int i;
    
    // === Setup input ring buffer =============================================
    ringL = ring_new(RING_SIZE);
    ringR = ring_new(RING_SIZE);
    
    // TODO debug sine table
    for (i = 0; i < 256; i++) {
        sintable[i] = (int) (32000.0 * sin((float)i * 6.283 / 256.0));
    }
  
    // === Setup DAC ===========================================================
    dac_setup();
    
    // === Setup SPI ===========================================================
    spi1_setup();
    
    ar_setup();
    
    // === setup system wide interrupts  =======================================
    INTEnableSystemMultiVectoredInt();
    
    mPORTASetPinsDigitalOut(BIT_0);
    mPORTAClearBits(BIT_0);
    
    unsigned short left_buf[AR_CHN_PKT_SIZE];
    unsigned short right_buf[AR_CHN_PKT_SIZE];
    
    while (1) {
        ar_receive(left_buf, right_buf);
        
        for (i = 0; i < AR_CHN_PKT_SIZE; i++) {
            ring_push(ringL, left_buf[i] );
            ring_push(ringR, right_buf[i]);
        }
        
        // Block while the number of unread packets is too high
        while (ring_size(ringL) > RING_SIZE / 2 || ring_size(ringR) > RING_SIZE / 2);
        
        // Heartbeat
        if (led_count++ % 20 == 0)
            mPORTAToggleBits(BIT_0);
        
    }
    
}