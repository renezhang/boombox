/**
 * Header file for SPI Setup
 **/
#ifndef __SPI_H__
#define __SPI_H__

#include <plib.h>

/***** Setup Parameters *****/

// SPI channels
#define DAC_CHANNEL SPI_CHANNEL1
#define REC_CHANNEL SPI_CHANNEL2

// Ratio between sampling rate and clock rate
#define SPI_DIV 4

/***** End Setup Parameter *****/

/***** Public Functions *****/

/* SPI Channel 1 - DAC Output */

// SCK1 - pin 25
// SDO1 - pin 22
// SS1 - pin 16
    
#define spi1_setup() { \
    mPORTBSetPinsDigitalOut(BIT_11); \
    PPSOutput(2, RPB11, SDO1); \
    mPORTBSetPinsDigitalOut(BIT_7); \
    PPSOutput(1, RPB7, SS1); \
    SpiChnOpenEx(SPI_CHANNEL1, SPI_OPEN_ON | SPI_OPEN_MODE16 | SPI_OPEN_MODE32 | SPI_OPEN_SSEN | SPI_OPEN_MSTEN | SPI_OPEN_CKP_HIGH | SPI_OPEN_MCLKSEL | SPI_OPEN_FSP_HIGH | SPI_OPEN_DISSDI, SPI_OPEN2_AUDEN | SPI_OPEN2_AUDMOD_I2S, SPI_DIV); \
    INTClearFlag(INT_SPI1TX); \
    INTSetVectorPriority(INT_SPI_1_VECTOR, INT_PRIORITY_LEVEL_4); \
    INTSetVectorSubPriority(INT_SPI_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0); \
    INTEnable(INT_SPI1, INT_ENABLED); \
    SpiChnPutC(SPI_CHANNEL1, 0); \
}

/* SPI Channel 2 - Audio data receive, slave mode */

// SCK2 - pin 26
// SDI2 - pin 12
#define spi2_setup() { \
    mPORTASetPinsDigitalIn(BIT_4); \
    PPSInput(3, SDI2, RPA4); \
    SpiChnOpen(SPI_CHANNEL2, SPI_OPEN_ON | SPI_OPEN_MODE8 | SPI_OPEN_SLVEN | SPI_OPEN_ENHBUF | SPI_OPEN_DISSDO, SPI_DIV); \
}

/***** End Public Functions *****/

#endif  /* __SPI_H__ */
