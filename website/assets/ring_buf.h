/*
 * File:   ring_buf.h
 *
 * Ring Buffer Library
 *
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * December 2015
 */
#ifndef __RING_H__
#define __RING_H__

typedef struct ring_buf* ring_buf_t;
/* The ring buffer holds unsigned characters */
typedef unsigned char out_t;

/* Constructs a new ring buffer with a given capacity on the heap */
extern ring_buf_t ring_new(unsigned int size);

/* Frees the ring buffer */
extern void ring_free(ring_buf_t);

/* Returns whether a ring buffer is empty or not */
extern unsigned int ring_empty(ring_buf_t);

/* Returns the current number of elements inside the ring buffer */
extern unsigned int ring_size(ring_buf_t);

/* Returns the maximum capacity of the ring buffer */
extern unsigned int ring_max_size(ring_buf_t);

/*
 * Pushes a single element into end of the ring buffer
 * This overrides the beginning of the buffer if necessary
 */
extern void ring_push(ring_buf_t, out_t);

/*
 * Pops an element off the beginning of the ring buffer
 * Returns 0 on failure
 */
extern out_t ring_pop(ring_buf_t);

#endif  /* __RING_H__ */
