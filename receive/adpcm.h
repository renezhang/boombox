/*
 * File:   adpcm.h
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

#ifndef __ADPCM_H__
#define	__ADPCM_H__

char ADPCMEncoder( signed long sample, signed long *prevsample, int *previndex);
signed long ADPCMDecoder( char code, signed long *prevsample, int *previndex );

#endif	/* __ADPCM_H__ */
