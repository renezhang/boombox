/**
 * Header file for audio data receive
 **/
#ifndef __AUDIO_RECEIVE_H__
#define __AUDIO_RECEIVE_H__

#include <plib.h>
#include "ring_buf.h"

/***** Public Constants *****/

// Number of bytes for each stereo channel.
// Total packet size is double this amount.
#define AR_CHN_PKT_SIZE 256

/***** Public Functions *****/

void ar_setup();

/**
 * 
 */
//void ar_receive(out_t *left_buffer, out_t *right_buffer);
void ar_receive(ring_buf_t ring);

/***** End Public Functions *****/

#endif  /* __AUDIO_RECEIVE_H__ */
