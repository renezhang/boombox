/*
 * Pragma config directives for configuring the PIC32.
 * 
 * File:   config.h
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

#ifndef __MCU_CONFIG_H__
#define	__MCU_CONFIG_H__

//==============================================================================

/*
 * WARNING: In order for the system to operate correctly, if clock settings have
 *          been changed, corresponding values in config.h must be changed.
 */

// 80 MHz
#pragma config FNOSC = FRCPLL, POSCMOD = OFF
#pragma config FPLLIDIV = DIV_2, FPLLMUL = MUL_20 // 40 MHz
#pragma config FPBDIV = DIV_1, FPLLODIV = DIV_2 // PB 40 MHz
#pragma config FWDTEN = OFF, JTAGEN = OFF, FSOSCEN = OFF, DEBUG = OFF

//==============================================================================

#endif	/* __MCU_CONFIG_H__ */
