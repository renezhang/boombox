///** 
// * Basic tests for UDP interface.
// *
// * Author: Cheng Xing
// */
//
//#include "mcu_config.h"
//#include "config.h"
//
//#include <string.h>
//#include "udp.h"
//#include "cli_uart.h"
//
//#define BUF_SIZE 256
//
//#define ASSERT_ON_ERROR(status) { if (status < 0) return status; }
//
//int send_test() {
//    int status = -1;
//    unsigned int msg[BUF_SIZE];
//    
//    // Clear the msg buffer
//    int i;
//    for (i = 0; i < BUF_SIZE; i++)
//        msg[i] = 0;
//    
//    status = udp_send_setup();
//    ASSERT_ON_ERROR(status);
//    
//    CLI_Write("Send Setup complete\r\n");
//    
////    strcpy(msg, "Hello World!\r\n");
//    msg[0] = 0x0101aefd;
//    status = udp_send_packet(msg, BUF_SIZE);
//    ASSERT_ON_ERROR(status);
//    
//    CLI_Write("Packet sent\r\n");
//    
//    return 0;
//}
//
//int receive_test() {
//    int status = -1;
//    unsigned int buf[BUF_SIZE];
//    char msg[30];
//    
//    // Clear the msg buffer
//    int i;
//    for (i = 0; i < BUF_SIZE; i++)
//        buf[i] = 0;
//    
//    status = udp_receive_setup();
//    ASSERT_ON_ERROR(status);
//    
//    CLI_Write("Receive Setup complete\r\n");
//    
//    while (1) {
//        status = udp_receive_packet(buf, BUF_SIZE);
//        sprintf(msg, "%x\n", buf[0]);
//        CLI_Write(msg);
//        ASSERT_ON_ERROR(status);
//
//        CLI_Write("Packet received\r\n");
//    }
//    
//    return 0;
//}
//
//int main() {
//    SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
//    ANSELA =0; //make sure analog is cleared
//    ANSELB =0;
//    
//    char screenbuf[30];
//    int status = receive_test();
//    
//    if (status < 0) {
//        sprintf(screenbuf, "UDP test failed: %d\r\n", status);
//        CLI_Write(screenbuf);
//    }
//}