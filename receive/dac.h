/**
 * Header file for DAC control
 **/
#ifndef __DAC_H__
#define __DAC_H__

#include <plib.h>
#include "config.h"

/***** Setup Parameters *****/

#define DAC_FREQ 44100
#define DAC_CHAN SPI_CHANNEL1
#define DAC_WORD_SIZE 4     // word size in bytes

#define DAC_CS_CLEAR() mPORTBClearBits(BIT_4)
#define DAC_CS_SET() mPORTBSetBits(BIT_4)

// A-channel, 1x, active
#define DAC_config_chan_A 0b0011000000000000

/***** End Setup Parameter *****/

/***** Public Functions *****/



// RPB13 - REFCLKO 11.2896 MHz
// REFCLKO = PBCLK / (2 * (N + M / 512))
// N = RODIV (last parameter of OSCREFConfig())
// M = ROTRIM (parameter of mOSCREFOTRIMSet())
// 395, 1
#define dac_setup() { \
    mPORTBSetPinsDigitalOut(BIT_13); \
    PPSOutput(3, RPB13, REFCLKO); \
    mOSCREFOTRIMSet(395); \
    OSCREFConfig(OSC_REFOCON_PBCLK, OSC_REFOCON_ON | OSC_REFOCON_OE, 1); \
}

#define dac_dma_setup(src_addr, src_len) { \
    DmaChnOpen(DMA_CHANNEL0, DMA_CHN_PRI0, DMA_OPEN_AUTO);              \
    DmaChnSetEventControl(DMA_CHANNEL0,                                 \
                          DMA_EV_START_IRQ(_SPI1_TX_IRQ));  \
    DmaChnSetTxfer(DMA_CHANNEL0,                                        \
                   src_addr, (void*)&SPI1BUF,                          \
                   src_len * DAC_WORD_SIZE, DAC_WORD_SIZE,              \
                   DAC_WORD_SIZE); \
    DmaChnEnable(DMA_CHANNEL0); \
}

/***** End Public Functions *****/

#endif  /* __DAC_H__ */
