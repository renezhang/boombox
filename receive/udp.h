/** 
 * Library for data transmission over UDP
 *
 * Author: Cheng Xing
 */

#ifndef _UDP_H
#define _UDP_H

#ifdef __cplusplus
extern "C" {
#endif
    
#define UDP_PORT 1337
#define RECEIVER_IP_HEX 0xc0a8006c // 192.168.0.108
    
    /**
     * Sets up the receiving UDP server.
     * Must be called before any other receive operations.
     */
    int udp_receive_setup();
    
    /**
     * Receives a single packet. This is a blocking operation.
     * The received packet is stored in message_buffer.
     * buffer_size - number of elements in the buffer.
     * Send buffer_size must equal receive buffer_size.
     */
    int udp_receive_packet(unsigned int * message_buffer, int buffer_size);
    
    /**
     * Sets up the transmitting UDP client
     * Must be called before any other send operations.
     */
    int udp_send_setup();
    
    /**
     * Sends a single packet stored in message_buffer.
     * buffer_size - number of elements in the buffer.
     * Send buffer_size must equal receive buffer_size.
     */
    int udp_send_packet(unsigned int * message_buffer, int buffer_size);
    
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */
