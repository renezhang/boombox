/**
 * Audio data receive implementation
 **/
#include "audio_receive.h"
//#include "spi.h"
#include "udp.h"

//#define ADPCM

/* Constants */

// AR_SYNC_WORD repeated AR_SYNC_REP times marks the start of a stereo packet.
#define AR_SYNC_WORD 0xDE 
#define AR_SYNC_REP  4

// ADPCM Decoding State Machines
signed long prev_sampleL = 0;
int         prev_indexL  = 0;
signed long prev_sampleR = 0;
int         prev_indexR  = 0;

/* Public functions */

void ar_setup() {
//    spi2_setup();
    udp_receive_setup();
}

//void ar_receive(out_t *left_buffer, out_t *right_buffer) {
void ar_receive(ring_buf_t ring) {
    
    // SPI wired transmission: Check for the start of a packet
    /*
    int sync_count = 0;
    int received;
    while (sync_count < AR_SYNC_REP) {
        SpiChnGetRov(REC_CHANNEL, 1);
        received = SpiChnGetC(REC_CHANNEL);
        if ( received == AR_SYNC_WORD)
            sync_count++;
        else
            sync_count = 0;
    }
     */
    
    // Read data into the two given buffers.
    // First half of the packet is for the left buffer, and second half is for
    // the right buffer.
    int i;
    unsigned short left_data, right_data;
//    for ( i = 0; i < AR_CHN_PKT_SIZE; i++) {
//        unsigned char encoded_data = (unsigned char) SpiChnGetC(REC_CHANNEL);
//        left_buffer[i]  = encoded_data >> 4;
//        right_buffer[i] = encoded_data & 0xF;
//    }
    // For raw data
    unsigned int packet[AR_CHN_PKT_SIZE];
#ifdef ADPCM
    signed short decoded_left, decoded_right;
    int j;
#endif
    // Clear the buffer
    for (i = 0; i < AR_CHN_PKT_SIZE; i++)
        packet[i] = 0;
    
    udp_receive_packet(packet, AR_CHN_PKT_SIZE);
    for (i = 0; i < AR_CHN_PKT_SIZE; i++) {
        // For SPI wired transmission
        // unsigned int encoded_data = (unsigned int) SpiChnGetC(REC_CHANNEL);
        
        // For UDP wireless transmission
        unsigned int encoded_data = packet[i];
        
        left_data  = (encoded_data >> 16);
        right_data = encoded_data & 0xFFFF;
        
#ifdef ADPCM
        for (j = 3; j >= 0; j--) {
            decoded_left  = 32768 + ADPCMDecoder((left_data >> (j*4)) & 0xF, &prev_sampleL, &prev_indexL);
            decoded_right = 32768 + ADPCMDecoder((right_data >> (j*4)) & 0xF, &prev_sampleR, &prev_indexR);
            ring_push(ring,
                    (unsigned long long) (decoded_left + 32768) << 40 |
                    (unsigned long long) (decoded_right + 32768) << 8);
        }
#else
        ring_push(ring,
            (unsigned long long) (left_data + 32768) << 40 |
            (unsigned long long) (right_data + 32768) << 8);
#endif
    }
    
}
