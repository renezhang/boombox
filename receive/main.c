#include "config.h"
#include "mcu_config.h"

#include "spi.h"
#include "ring_buf.h"
#include "audio_receive.h"
#include "dac.h"
#include "adpcm.h"

// Ring buffer, for buffering between data reception and DAC
#define RING_SIZE 1024
// Circular ring buffer for audio
//volatile static ring_buf_t ringL;
//volatile static ring_buf_t ringR;
volatile static ring_buf_t ring;

// DAC ISR variables
volatile int DAC_LR = 0;
volatile unsigned int readL = 0;
volatile unsigned int readR = 0;

// ADPCM Decoding State Machines
//signed long prev_sampleL = 0;
//int         prev_indexL  = 0;
//signed long prev_sampleR = 0;
//int         prev_indexR  = 0;

// Heartbeat
volatile int led_count = 0;

// ipl4
//void __ISR(_SPI_1_VECTOR, IPL4AUTO) SPI1InterruptHandler(void) {
//  if (DAC_LR) {
////    if (!ring_empty(ringL)) {
////      readL = ring_pop(ringL);
////      readL = ADPCMDecoder(readL, &prev_sampleL, &prev_indexL);
//      
//    // Raw data
//    if (!ring_empty(ringL)) {
//        readL = ring_pop(ringL);
//        readL += 32768; // DAC takes in a two's complement value.
//        SpiChnWriteC(DAC_CHAN, readL << 8);
//    } else {
//        SpiChnWriteC(DAC_CHAN, 0);
//    }
//  } else {
////    if (!ring_empty(ringR)) {
////      readR = ring_pop(ringR);
////      readR = ADPCMDecoder(readR, &prev_sampleR, &prev_indexR);
//      
//    // Raw data
//    if (!ring_empty(ringR)) {
//      readR = ring_pop(ringR);
//      readR += 32768; // DAC takes in a two's complement value.
//      SpiChnWriteC(DAC_CHAN, readR << 8); 
//    } else {
//        SpiChnWriteC(DAC_CHAN, 0);
//    }
//  }
//  DAC_LR = 1 - DAC_LR;
//  // Clear interrupt
//  INTClearFlag(INT_SPI1TX);
//}

// TODO remove
    unsigned short sine_table[64] = {
32768,36030,39260,42426,45496,
48439,51226,53830,56225,58386,
60293,61926,63270,64310,65037,
65443,65525,65281,64713,63829,
62635,61145,59373,57336,55055,
52553,49854,46985,43975,40853,
37651,34401,31134,27884,24682,
21560,18550,15681,12982,10480,
8199,6162,4390,2900,1706,
822,254,10,92,498,
1225,2265,3609,5242,7149,
9310,11705,14309,17096,20039,
23109,26275,29505,32768,
    };

void main() {
    
    SYSTEMConfig(sys_clock, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
  
    ANSELA =0; //make sure analog is cleared
    ANSELB =0;
    
    int i;
    
    // === Setup input ring buffer =============================================
//    ringL = ring_new(RING_SIZE);
//    ringR = ring_new(RING_SIZE);
    ring = ring_new(RING_SIZE);
    
    // === Setup Audio Data Receive ============================================
    ar_setup();
//    out_t left_buf[AR_CHN_PKT_SIZE];
//    out_t right_buf[AR_CHN_PKT_SIZE];
//    // Clear the buffers
//    for (i = 0; i < AR_CHN_PKT_SIZE; i++)
//        left_buf[i] = right_buf[i] = 0;
    
    dac_dma_setup(ring->data, ring->size-1);
    
    // === Setup DAC ===========================================================
    dac_setup();
    
    // === Setup SPI ===========================================================
    spi1_setup();
    
    // === setup system wide interrupts  =======================================
    INTEnableSystemMultiVectoredInt();
    
    // === Setup Heartbeat =====================================================
    mPORTASetPinsDigitalOut(BIT_0);
    mPORTAClearBits(BIT_0);
    
    while (1) {
        
        // Receive audio data
//        CLI_Write("Receiving data...");
        ar_receive(ring);
//        CLI_Write("Received");
//        char msg[30];
//        CLI_Write("Left buffer");
//        for (i = 0; i < AR_CHN_PKT_SIZE; i++) {
//            sprintf(msg, "%d", left_buf[i]);
//            CLI_Write(msg);
//        }
//        CLI_Write("Right buffer");
//        for (i = 0; i < AR_CHN_PKT_SIZE; i++) {
//            sprintf(msg, "%d", right_buf[i]);
//            CLI_Write(msg);
//        }
        
        // Push received data into ring buffer
//        for (i = 0; i < AR_CHN_PKT_SIZE; i++) {
////            ring_push(ringL, left_buf[i] << 8);
////            ring_push(ringR, right_buf[i] << 8);
////            ring_push(ringL, sine_table[i%64] << 8);
////            ring_push(ringR, sine_table[i%64] << 8);
////            ring_push(ring, 
////                    (unsigned long long) (sine_table[i%64] + 32768) << 40 |
////                    (unsigned long long) (sine_table[i%64] + 32768) << 8);
//            ring_push(ring, 
//                    (unsigned long long) (left_buf[i] + 32768) << 40 |
//                    (unsigned long long) (right_buf[i] + 32768) << 8);
//        }
        
        // Block while the number of unread packets is too high (for SPI wired transmission)
//        while (ring_size(ringL) > RING_SIZE / 2 || ring_size(ringR) > RING_SIZE / 2);
        
        
        // Heartbeat
        if (led_count++ % 20 == 0)
            mPORTAToggleBits(BIT_0);
    }
    
}