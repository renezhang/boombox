/* ************************************************************************** */
/**

  @File Name
    cc3100.c

  @Description
    Contains the driver parameters for CC3100 SimpleLink library to function.
 */
/* ************************************************************************** */

#include "cc3100.h"

#define DMA_CHANNEL DMA_CHANNEL1
//#define DMA_ENABLED

P_EVENT_HANDLER pIraEventHandler = 0;

channel_t cc3100_spi_open(char * protocol_name, unsigned long flags) {
    
    if (CC3100_SPI_CH == SPI_CHANNEL1) {
        mPORTBSetPinsDigitalIn(BIT_5);
        PPSInput(2, SDI1, RPB5);
        mPORTBSetPinsDigitalOut(BIT_1);
        PPSOutput(2, RPB1, SDO1);
    } else { // SPI_CH == SPI_CHANNEL2
        mPORTASetPinsDigitalIn(BIT_4);
        PPSInput(3, SDI2, RPA4);
        mPORTASetPinsDigitalOut(BIT_1);
        PPSOutput(2, RPA1, SDO2);
    }
    
    SpiChnOpen(
        CC3100_SPI_CH, 
        SPI_OPEN_ON | SPI_OPEN_MODE32 | SPI_OPEN_MSTEN | SPI_OPEN_ENHBUF,
        CC3100_SPI_DIV
    );
    
#ifdef DMA_ENABLED
    DmaChnOpen(DMA_CHANNEL0, DMA_CHN_PRI0, DMA_OPEN_AUTO);
#endif
    
    return CC3100_SPI_CH;
    
}

int cc3100_spi_close(channel_t channel) {
    SpiChnClose(channel);
}

int cc3100_spi_read (channel_t channel, unsigned char * buffer, int len) {
    // Casts the char buffer to an int buffer
    int i;
    int spi_len = len / 4;
    unsigned int temp[spi_len];
    
    cc3100_cs_clear();
    
    // Given the CC3100 Host Interface spec, there shouldn't be a case
    // where len is not divisible by 4.
    for (i = 0; i < spi_len; i++) {
        SpiChnPutC(channel, 0xFF);
        temp[i] = SpiChnGetC(channel);
    }
    
    if (channel == SPI_CHANNEL1)
        while (SPI1STATbits.SPIBUSY);
    else // channel == SPI_CHANNEL2
        while (SPI2STATbits.SPIBUSY);
    cc3100_cs_set();
    
    /* SPI read with 8-bit words
    for (i = 0; i < len; i++) {
        buffer[i] = (unsigned char) temp[i];
    }
     */
    // SPI read with 32-bit words
    for (i = 0; i < spi_len; i++) {
        // Truncation of the most significant bytes
        // is done automatically by the cast.
        buffer[4*i]     = (unsigned char) (temp[i] >> 24);
        buffer[4*i + 1] = (unsigned char) (temp[i] >> 16);
        buffer[4*i + 2] = (unsigned char) (temp[i] >> 8);
        buffer[4*i + 3] = (unsigned char) temp[i];
    }
    
    // Debug print-out for read value
//    char *txt_buf[20];
//    for (i = 0; i < len; i++) {
//        sprintf(txt_buf, "READ: 0x%x", buffer[i]);
//        CLI_Write(txt_buf);
//    }
    
    return len;
}

int cc3100_spi_write(channel_t channel, unsigned char * buffer, int len) {
    // Casts the char buffer to an int buffer
    int i;
    unsigned int data;
    
    /* SPI Write with 8-bit words
    cc3100_cs_clear();
    for (i = 0; i < len; i++) {
        SpiChnPutC(channel, buffer[i]);
        SpiChnGetC(channel);
    }
    if (channel == SPI_CHANNEL1)
        while (SPI1STATbits.SPIBUSY);
    else // channel == SPI_CHANNEL2
        while (SPI2STATbits.SPIBUSY);
    cc3100_cs_set();
     */
    
     // Debug print-out for read value
//    char *txt_buf[20];
//    for (i = 0; i < len; i++) {
//        sprintf(txt_buf, "SENT: 0x%x", buffer[i]);
//        CLI_Write(txt_buf);
//    }
    
    // SPI Write with 32-bit words
    cc3100_cs_clear();
    for (i = 0; i < len; i += 4) {
        // Given the CC3100 Host Interface spec, there shouldn't be a case
        // where len is not divisible by 4.
        data = (unsigned int) buffer[i]   << 24 |
               (unsigned int) buffer[i+1] << 16 |
               (unsigned int) buffer[i+2] << 8  |
               (unsigned int) buffer[i+3];
        SpiChnPutC(channel, data);
        SpiChnGetC(channel);
    }
    if (channel == SPI_CHANNEL1)
        while (SPI1STATbits.SPIBUSY);
    else // channel == SPI_CHANNEL2
        while (SPI2STATbits.SPIBUSY);
    cc3100_cs_set();
    
    return len;
}

int registerInterruptHandler(P_EVENT_HANDLER InterruptHdl , void* pValue)
{
    pIraEventHandler = InterruptHdl;

    return 0;
}

/*
 * External interrupt handler for CC3100 IRQ.
 */
// ipl1
void __ISR(_EXTERNAL_0_VECTOR, IPL2AUTO) cc3100IRQHandler(void) {
  if (pIraEventHandler)  
    pIraEventHandler(0);
  // Clear interrupt
  mINT0ClearIntFlag();
}