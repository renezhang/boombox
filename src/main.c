/**
 * Boombox
 * Cheng Xing, Rene Zhang
 * cx34, rz99
 * September 2015
 */

// === Config ==================================================================
// clock AND protoThreads configure!
#include "config.h"
// threading library
#include "pt_cornell_1_2.h"

// === Libraries ===============================================================
// Standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
// Graphics libraries
#include "tft_master.h"
#include "tft_gfx.h"

// Ring buffer library
#include "ring_buf.h"

// Audio hardware libraries
#include "dac.h"
#include "adc.h"

// === Math Macros =============================================================
#define clamp(v, a, b) (v < a ? a : (v > b ? b : v))

// === TFT Macros ==============================================================
#define SIZE_TO_HEIGHT 9
#define tft_clearRect(x, y, w, h) { tft_fillRect(x, y, w, h, ILI9340_BLACK); }
#define tft_writeLine(x, y, size, s) { tft_setCursor(x, y); tft_setTextSize(size); tft_writeString(s); }
#define tft_rewriteLine(x, y, w, size, format, var) { \
  sprintf(buf, format, var); \
  tft_clearRect(x, y, w, size * SIZE_TO_HEIGHT); \
  tft_writeLine(x, y, size, buf); \
}

// === Thread Structures =======================================================
// thread control structs
static struct pt pt_screen;

// === Variables ===============================================================
// screen buffer
static char buf[80];

// iterator
static int i;

#define RING_SIZE 4096
// Circular ring buffer for audio
volatile static ring_buf_t ring;

// === Screen Thread ===========================================================
/**
 * Thread used to refresh screen information
 */
static PT_THREAD (protothread_screen(struct pt *pt)) {
  PT_BEGIN(pt);

  while(1) {
//    tft_rewriteLine(0, 0, 240, 2, "%d", adc_read());
    tft_rewriteLine(0, 0, 240, 2, "Ring Size: %d", ring_size(ring));

    PT_YIELD_TIME_msec(200);

    // NEVER exit while
  } // END WHILE(1)
  PT_END(pt);
} // screen thread

// === Timer 2 Interrupt Handler ===============================================
/*
 * Interrupt handler for timer 2.
 */

unsigned short read;

void __ISR(_TIMER_2_VECTOR, ipl3) Timer2Handler(void) {
  // Clear interrupt
  mT2ClearIntFlag();

  if (!ring_empty(ring)) {
    read = ring_pop(ring);
    dac_output(clamp(read, 0, 4096));
  }
}

// === Timer 3 Interrupt Handler ===============================================
/*
 * Interrupt handler for timer 3.
 */

void __ISR(_TIMER_3_VECTOR, ipl2) Timer3Handler(void) {
  // Clear interrupt
  mT3ClearIntFlag();
  ring_push(ring, adc_read() * 4);
}

// === Main  ===================================================================
/*
 * Set up TFT and other peripherals.
 * Set and schedule up protothreads.
 */
int main(void) {
  // === Setup the timer5 ISR ==================================================
  PT_setup();

  // === Setup the TFT =========================================================
  tft_init_hw();
  tft_begin();
  tft_fillScreen(ILI9340_BLACK);
  tft_setRotation(0); // 240x320 vertical display
  tft_setTextColor(ILI9340_WHITE);

  // === Setup input ring buffer ===============================================
  ring = ring_new(RING_SIZE);
  
  // === Setup timer2 ==========================================================
  OpenTimer2(T2_ON | T2_SOURCE_INT | T2_PS_1_1, sys_clock / DAC_FREQ);

  ConfigIntTimer2(T2_INT_ON | T2_INT_PRIOR_3);
  mT2ClearIntFlag();
  
  // === Setup timer3 ==========================================================
  OpenTimer3(T3_ON | T3_SOURCE_INT | T3_PS_1_1, sys_clock / ADC_FREQ);

  ConfigIntTimer3(T3_INT_ON | T3_INT_PRIOR_2);
  mT3ClearIntFlag();

  // == Setup DAC ==============================================================
  dac_setup();

  // === Setup ADC =============================================================
  adc_setup();

  // === Setup protothreads ====================================================
  PT_INIT(&pt_screen);

  // === setup system wide interrupts  =========================================
  INTEnableSystemMultiVectoredInt();

  // === Schedule the threads round robin ======================================
  while(1) {
    PT_SCHEDULE(protothread_screen(&pt_screen));
  }
} // main
