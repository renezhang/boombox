/**
 * Header file for DAC control
 **/
#ifndef __DAC_H__
#define __DAC_H__

#include <plib.h>

/***** Setup Parameters *****/

// SPI Channel
#define SPI_CH SPI_CHANNEL2

// Ratio between sampling rate and clock rate
#define SPI_DIV 2
#define DAC_FREQ 44100

// A-channel, 1x, active
#define DAC_config_chan_A 0b0011000000000000

/***** End Setup Parameter *****/

/***** Public Functions *****/

#define dac_output(data) { \
    mPORTBClearBits(BIT_4); \
    while (TxBufFullSPI2()); \
    WriteSPI2(DAC_config_chan_A | data); \
    while (SPI2STATbits.SPIBUSY); \
    mPORTBSetBits(BIT_4); \
}

// DAC Peripheral Setup
  // SCK2 is pin 26
  // SDO2 (MOSI) is in PPS output group 2, could be connected to RB5 which is pin 14
#define dac_setup() { \
    PPSOutput(2, RPB5, SDO2); \
    mPORTBSetPinsDigitalOut(BIT_4); \
    mPORTBSetBits(BIT_4); \
    SpiChnOpen(SPI_CH, SPI_OPEN_ON | SPI_OPEN_MODE16 | SPI_OPEN_MSTEN | SPI_OPEN_CKE_REV, SPI_DIV); \
}

/***** End Public Functions *****/

#endif  /* __DAC_H__ */
