/**
 * Header file for ADC control
 **/
#ifndef __ADC_H__
#define __ADC_H__

#include <plib.h>

/***** Setup Parameters *****/

// define setup parameters for OpenADC10
// Turn module on | ouput in integer | trigger mode auto | enable autosample
// ADC_CLK_AUTO -- Internal counter ends sampling and starts conversion (Auto convert)
// ADC_AUTO_SAMPLING_ON -- Sampling begins immediately after last conversion completes; SAMP bit is automatically set
// ADC_AUTO_SAMPLING_OFF -- Sampling begins with AcquireADC10();
#define PARAM1  ADC_FORMAT_INTG16 | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON

// define setup parameters for OpenADC10
// ADC ref external  | disable offset test | disable scan mode | do 1 sample | use single buf | alternate mode off
#define PARAM2  ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_1 | ADC_ALT_BUF_OFF | ADC_ALT_INPUT_OFF

// Define setup parameters for OpenADC10
// use peripherial bus clock | set sample time | set ADC clock divider
// ADC_CONV_CLK_Tcy2 means divide CLK_PB by 2 (max speed)
// ADC_SAMPLE_TIME_5 seems to work with a source resistance < 1kohm
#define PARAM3 ADC_CONV_CLK_PB | ADC_SAMPLE_TIME_5 | ADC_CONV_CLK_Tcy2 //ADC_SAMPLE_TIME_15| ADC_CONV_CLK_Tcy2

// define setup parameters for OpenADC10
// set AN4 and  as analog inputs
#define PARAM4  ENABLE_AN11_ANA // pin 24

// define setup parameters for OpenADC10
// do not assign channels to scan
#define PARAM5  SKIP_SCAN_ALL

/***** End Setup Parameter *****/

/***** Public Functions *****/

#define ADC_FREQ 44100

// Reads ADC value
#define adc_read() ReadADC10(0)

unsigned short data;

//unsigned short adc_read() {
//    mPORTBClearBits(BIT_8);
//    while (TxBufFullSPI2());
//    WriteSPI2(1);
//    while (SPI2STATbits.SPIBUSY);
////    while (!DataRdySPI2());
//    data = ReadSPI2();
//    mPORTBSetBits(BIT_8);
//    return data;
//}

// ADC Peripheral Setup
#define adc_setup() { \
    PPSInput(3, SDI2, RPB6); \
    mPORTBSetPinsDigitalOut(BIT_8); \
    mPORTBSetBits(BIT_8); \
    CloseADC10(); \
    SetChanADC10( ADC_CH0_NEG_SAMPLEA_NVREF | ADC_CH0_POS_SAMPLEA_AN11 ); \
    OpenADC10( PARAM1, PARAM2, PARAM3, PARAM4, PARAM5 ); \
    EnableADC10(); \
}

/***** End Public Functions *****/

#endif  /* __ADC_H__ */
